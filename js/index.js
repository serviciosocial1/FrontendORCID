const searchButton = document.getElementById("searchButton");
const searchInput = document.getElementById("searchInput");
const divCard = document.getElementById("card");

// Get ORCID
function getOrcid(value) {
  // console.log('hola');
  if (value !== "") {
    return fetch(`https://pub.orcid.org/v3.0/${value}`, {
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  }
}

// Event click search
searchButton.addEventListener("click", function () {
  if (searchInput.value !== "") processSearch();
});

// Event key enter
searchInput.addEventListener("keyup", (event) => {
  if (event.key === "Enter" && searchInput.value !== "") processSearch();
});

// Search process
function processSearch() {
  getOrcid(searchInput.value)
    .then(function (data) {
      console.log(data);
      //  limpia barra de busqueda
      searchInput.value = "";
      clearCard(divCard);
      buildCard(data);
      console.log(data);
    })
    .catch(function () {
      //   let title = document.createElement("h3");
      //   title.textContent = "Not Found!";
      //   card.appendChild();
    });
}

function buildCard(data) {
  card = document.createElement("div");
  imageCard = document.createElement("img");
  cardBody1 = document.createElement("div");
  listCard = document.createElement("ul");
  titleCard = document.createElement("h5");
  textCard = document.createElement("p");
  cardBody2 = document.createElement("div");
  linkCard = document.createElement("a");

  card.className = "card";
  card.style = "width: 20rem;";
  imageCard.className = "card-img-top";
  imageCard.src = "user.png";
  cardBody1.className = "card-body";
  cardBody2.className = "card-body";
  listCard.className = "list-group list-group-flush";
  titleCard.className = "card-title";
  textCard.className = "card-text";
  linkCard.className = "card-link";
  linkCard.textContent = "Perfil ORCID";
  linkCard.target = "_blank";

  // Nombre y perfil
  textCard.textContent = `Total publicaciones: ${data["activities-summary"].works.group.length}`;
  let name = data.person.name["given-names"].value;
  let lastName = data.person.name["family-name"].value;
  let uri = data["orcid-identifier"].uri;
  titleCard.textContent = `${name} ${lastName}`;
  linkCard.href = `${uri}`;

  // console.log(name, lastName, uri);

  // console.log(data["activities-summary"].works.group.length);

  
  //   Publicaciones
  for (let i = 0; i < data["activities-summary"].works.group.length; i++) {
    let workCard = document.createElement("li");
    let listWork = document.createElement("ul");

    workCard.className = "list-group-item";

    // title Work
    let li1 = document.createElement("li");
    if (
      data["activities-summary"].works.group[i]["work-summary"][0].title.title
    ) {
      let titleWork =
        data["activities-summary"].works.group[i]["work-summary"][0].title.title
          .value;
      li1.textContent = `Title: ${titleWork}`;
    } else li1.textContent = "Title: -";
    listWork.appendChild(li1);

    // Journal title
    let li2 = document.createElement("li");
    if (
      data["activities-summary"].works.group[i]["work-summary"][0][
        "journal-title"
      ]
    ) {
      let journalTitle =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "journal-title"
        ].value;
      li2.textContent = `Journal title: ${journalTitle}`;
    } else li2.textContent = "Journal title: -";
    listWork.appendChild(li2);

    // //  Publication Date
    if (
      data["activities-summary"].works.group[i]["work-summary"][0][
        "publication-date"
      ].day &&
      data["activities-summary"].works.group[i]["work-summary"][0][
        "publication-date"
      ].month &&
      data["activities-summary"].works.group[i]["work-summary"][0][
        "publication-date"
      ].year
    ) {
      let li3 = document.createElement("li");
      let dayWork =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "publication-date"
        ].day.value;
      let monthWork =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "publication-date"
        ].month.value;
      let yearWork =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "publication-date"
        ].year.value;
      li3.textContent = `Publication date: ${dayWork}-${monthWork}-${yearWork}`;
      listWork.appendChild(li3);
    } else if (
      data["activities-summary"].works.group[i]["work-summary"][0][
        "publication-date"
      ].month &&
      data["activities-summary"].works.group[i]["work-summary"][0][
        "publication-date"
      ].year
    ) {
      let li3 = document.createElement("li");
      let monthWork =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "publication-date"
        ].month.value;
      let yearWork =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "publication-date"
        ].year.value;
      li3.textContent = `Publication date: ${monthWork}-${yearWork}`;
      listWork.appendChild(li3);
    } else if (
      data["activities-summary"].works.group[i]["work-summary"][0][
        "publication-date"
      ].year
    ) {
      let li3 = document.createElement("li");
      let yearWork =
        data["activities-summary"].works.group[i]["work-summary"][0][
          "publication-date"
        ].year.value;
      li3.textContent = `Publication date: ${yearWork}`;
      listWork.appendChild(li3);
    }

    //  Link Publication
    if (data["activities-summary"].works.group[i]["work-summary"][0].url) {
      let li4 = document.createElement("li");
      let linkWork = document.createElement("a");
      let linkPageWork =
        data["activities-summary"].works.group[i]["work-summary"][0].url.value;

      linkWork.className = "card-link";
      linkWork.textContent = "Link publicacion";
      linkWork.target = "_blank";
      linkWork.href = linkPageWork;
      li4.appendChild(linkWork);
      listWork.appendChild(li4);
    }

    listCard.appendChild(workCard);
    workCard.appendChild(listWork);
  }

  divCard.appendChild(card);
  card.appendChild(imageCard);
  card.appendChild(cardBody1);
  card.appendChild(listCard);
  cardBody1.appendChild(titleCard);
  cardBody1.appendChild(textCard);
  card.appendChild(cardBody2);
  cardBody2.appendChild(linkCard);
}

// Funcion para vaciar nodo
function clearCard(nodo) {
  Array.from(nodo.children).forEach((children) => nodo.removeChild(children));
}
