// Elementos del DOM
const titleList = document.getElementById("titleListPerfiles");
const divList = document.getElementById("listPerfil");
const selector = document.getElementById("selector");
const publicaciones = document.getElementById("publicaciones");
// const perfilOrcid = document.getElementById("perfilOrcid");

//
const listResults = [];
let maxWorks = 5;

// Get ORCID
function getOrcid(value) {
  return fetch(`https://pub.orcid.org/v3.0/${value}`, {
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
}

// Peticion a Backend (All)
function getPersonas() {
  return fetch("http://127.0.0.1:3000/personas").then((response) =>
    response.json()
  );
}

// Peticion a Backend (ID)
function getPersona(id) {
  return fetch(`http://127.0.0.1:3000/personas/${id}`).then((response) =>
    response.json()
  );
}

// Evento select
selector.addEventListener("change", function () {
  let deptoValue = "";
  switch (selector.value) {
    case "0":
      clearListsResults();
      titleList.textContent = ``;
      break;
    case "1":
      deptoValue = "Biología Celular y Fisiología";
      clearListsResults();
      dataProcess(deptoValue);
      break;
    case "2":
      deptoValue = "Biología Molecular y Biotecnología";
      clearListsResults();
      dataProcess(deptoValue);
      break;
    case "3":
      deptoValue = "Inmunología";
      clearListsResults();
      dataProcess(deptoValue);
      break;
    case "4":
      deptoValue = "Medicina Genómica y Toxicología Ambiental";
      clearListsResults();
      dataProcess(deptoValue);
      break;
    default:
      break;
  }
});

// Procesado de peticion
function dataProcess(depto) {
  getPersonas()
    .then(function (data) {
      buildListPerfiles(filterDepto(data, depto));
      eventModal();
      titleList.textContent = `${
        filterDepto(data, depto).length
      } Resultados para ${depto}`;
    })
    .catch(function () {
      titleList.textContent = `Sin Resultados`;
      console.log("Error de peticion Backend");
    });
}

// Event modal
function eventModal() {
  listResults.forEach(function (cardPerfil) {
    cardPerfil.addEventListener("click", function () {
      getPersona(cardPerfil.id).then((data) => buildModal(data));
    });
  });
}

// Funcion para construir Modal
function buildModal(data) {
  let modalTitle = document.getElementById("modalTitle");
  let imgPerfil = document.getElementById("imgPerfil");
  let departamento = document.getElementById("departamento");
  let worksList = document.getElementById("worksList");
  let publicaciones = document.getElementById("publicaciones");

  modalTitle.textContent = data.nombre;
  imgPerfil.src = data.urlImagen;
  departamento.textContent = `Departamento: ${data.depto}`;
  publicaciones.textContent = "";
  clearNodo(worksList);
  // perfilOrcid.style.display = "none";

  // validar si tiene asociado un orcid
  if (data.orcid === "Sin datos") {
    publicaciones.textContent = "Sin Datos ORCID";
  } else {
    clearNodo(worksList);
    orcidProcess(worksList, data.orcid);
  }
}

function orcidProcess(list, orcid) {
  getOrcid(orcid)
    .then(function (data) {
      publicaciones.textContent = `Total publicaciones: ${data["activities-summary"].works.group.length}`;
      // perfilOrcid.href = `${data["orcid-identifier"].uri}`;
      // perfilOrcid.style.display = "block";
      let limitWorks = 0;
      if (data["activities-summary"].works.group.length >= maxWorks)
        limitWorks = maxWorks;
      else limitWorks = data["activities-summary"].works.group.length;

      //   Publicaciones
      for (let i = 0; i < limitWorks; i++) {
        let elementWork = document.createElement("li");
        let descripWork = document.createElement("ul");

        elementWork.className = "list-group-item";

        // title Work and Link Publication
        let li1 = document.createElement("li");
        const linkWork = document.createElement("a");
        if (
          data["activities-summary"].works.group[i]["work-summary"][0].title
            .title
        ) {
          let titleWork =
            data["activities-summary"].works.group[i]["work-summary"][0].title
              .title.value;
          linkWork.textContent = `${titleWork}`;
        } else linkWork.textContent = "Title: -";
        if (data["activities-summary"].works.group[i]["work-summary"][0].url) {
          let linkPageWork =
            data["activities-summary"].works.group[i]["work-summary"][0].url
              .value;
          linkWork.className = "card-link";
          linkWork.target = "_blank";
          linkWork.href = linkPageWork;
        }
        li1.appendChild(linkWork);
        descripWork.appendChild(li1);

        // Journal title
        let li2 = document.createElement("li");
        if (
          data["activities-summary"].works.group[i]["work-summary"][0][
            "journal-title"
          ]
        ) {
          let journalTitle =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "journal-title"
            ].value;
          li2.textContent = `Journal title: ${journalTitle}`;
        } else li2.textContent = "Journal title: -";
        descripWork.appendChild(li2);

        // //  Publication Date
        if (
          data["activities-summary"].works.group[i]["work-summary"][0][
            "publication-date"
          ].day &&
          data["activities-summary"].works.group[i]["work-summary"][0][
            "publication-date"
          ].month &&
          data["activities-summary"].works.group[i]["work-summary"][0][
            "publication-date"
          ].year
        ) {
          let li3 = document.createElement("li");
          let dayWork =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "publication-date"
            ].day.value;
          let monthWork =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "publication-date"
            ].month.value;
          let yearWork =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "publication-date"
            ].year.value;
          li3.textContent = `Publication date: ${dayWork}-${monthWork}-${yearWork}`;
          descripWork.appendChild(li3);
        } else if (
          data["activities-summary"].works.group[i]["work-summary"][0][
            "publication-date"
          ].month &&
          data["activities-summary"].works.group[i]["work-summary"][0][
            "publication-date"
          ].year
        ) {
          let li3 = document.createElement("li");
          let monthWork =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "publication-date"
            ].month.value;
          let yearWork =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "publication-date"
            ].year.value;
          li3.textContent = `Publication date: ${monthWork}-${yearWork}`;
          descripWork.appendChild(li3);
        } else if (
          data["activities-summary"].works.group[i]["work-summary"][0][
            "publication-date"
          ].year
        ) {
          let li3 = document.createElement("li");
          let yearWork =
            data["activities-summary"].works.group[i]["work-summary"][0][
              "publication-date"
            ].year.value;
          li3.textContent = `Publication date: ${yearWork}`;
          descripWork.appendChild(li3);
        }
        list.appendChild(elementWork);
        elementWork.appendChild(descripWork);

        if (
          data["activities-summary"].works.group.length >= maxWorks &&
          i === limitWorks - 1
        ) {
          let li4 = document.createElement("li");
          const linkPerfil = document.createElement("a");

          linkPerfil.textContent = "Read more...";
          linkPerfil.className = "card-link";
          linkPerfil.target = "_blank";
          linkPerfil.href = `${data["orcid-identifier"].uri}`;

          li4.appendChild(linkPerfil);
          list.appendChild(li4);
        }
      }
    })
    .catch();
}

// Funcion para construir lista de resultados
function buildListPerfiles(data) {
  Array.from(data).forEach((perfil) => {
    let divCol = document.createElement("div");
    let divCard = document.createElement("div");
    let cardImage = document.createElement("img");
    let cardBody = document.createElement("div");
    let titleBody = document.createElement("h5");

    divCol.className = "col";
    divCard.className = "card h-100";
    divCard.id = `${perfil.id}`;
    divCard.dataset.bsToggle = "modal";
    divCard.dataset.bsTarget = "#modalPerfil";
    listResults.push(divCard);
    cardImage.className = "card-img-top";
    cardImage.src = perfil.urlImagen;

    cardBody.className =
      "card-body d-flex justify-content-center align-items-center";
    titleBody.className = "card-title text-center";
    divList.appendChild(divCol);
    divCol.appendChild(divCard);
    divCard.appendChild(cardImage);
    divCard.appendChild(cardBody);
    cardBody.appendChild(titleBody);
    titleBody.appendChild(document.createTextNode(perfil.nombre));
  });
}

// Filtrar por Depto
function filterDepto(data, depto) {
  let datafilter = [];
  Array.from(data).forEach(function (person) {
    if (person.depto === depto) datafilter.push(person);
  });
  return datafilter;
}

// Funcion para vaciar lista de resultados
function clearListsResults() {
  Array.from(divList.children).forEach((divCol) => divList.removeChild(divCol));
  listResults.splice(0);
}
// Funcion para vaciar nodo
function clearNodo(nodo) {
  Array.from(nodo.children).forEach((children) => nodo.removeChild(children));
}
